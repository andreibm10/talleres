/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.cr.ulacit.javaessentials.semana3.extraclase2;

import javax.swing.JOptionPane;

/**
 *
 * @author Dev1_JD
 */
public class Estudiantes {

    public static void main(String[] args) {
        int menor = 0;
        int mayor = 0;
        int promedio = 0;
        int cant = 5;
        for (int i = 0; i < cant; i++) {
            int nota = Integer.parseInt(JOptionPane.showInputDialog("Digite la nota: "));
            if (nota == -1) {
                break;
            }
            if (i == 0) {
                menor = nota;
            } else {
                if (nota < menor) {
                    menor = nota;
                }
            }
            if (nota > mayor) {
                mayor = nota;
            }
            promedio = promedio + nota;
        }

        System.out.println("La nota menor " + menor);
        System.out.println("LA nota mayor " + mayor);
        System.out.println("El promedio es " + (promedio / cant));
    }

}
