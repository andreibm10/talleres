/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.cr.ulacit.javaessentials.semana3.ejericicio1;

/**
 *
 * @author Dev1_JD
 */
public class Practica {

    public void ejemplo01(int d) {
        String dia;
        switch (d) {
            case 1:
                dia = "Domingo";
                break;
            case 2:
                dia = "Lunes";
                break;
            case 3:
                dia = "Martes";
                break;
            case 4:
                dia = "Miercoles";
                break;
            case 5:
                dia = "Jueves";
                break;
            case 6:
                dia = "Viernes";
                break;
            case 7:
                dia = "Sabado";
                break;
            default:
                dia = "Dia invalido";
                break;
        }
        System.out.println("El dias es " + dia);
    }
}
