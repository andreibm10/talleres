/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.cr.ulacit.javaessentials.semana3.extraclase1;

import javax.swing.JOptionPane;

/**
 *
 * @author Dev1_JD
 */
public class TipoHora {

    public static void main(String args[]) {
        int hora = Integer.parseInt(JOptionPane.showInputDialog("Digite la hora"));
        tipoHora(hora);
    }

    private static void tipoHora(int hora) {
        String tipoHora = "";
        int minutos = hora % 100;
        if (minutos > 59) {
            JOptionPane.showMessageDialog(null, "Error Minutos Invalidos");
        } else {
            if (hora >= 1 && hora <= 559) {
                tipoHora = "Madrugada";
            } else if (hora >= 600 && hora <= 1159) {
                tipoHora = "Manana";
            } else if (hora == 1200) {
                tipoHora = "Mediodia";
            } else if (hora >= 1201 && hora <= 1759) {
                tipoHora = "Tarde";
            } else if (hora >= 1800 && hora <= 2359) {
                tipoHora = "Noche";
            } else if (hora == 2400) {
                tipoHora = "MediaNoche";
            } else {
                JOptionPane.showMessageDialog(null, "Error Hora Invalida");
            }
            if (!tipoHora.equals("")) {
                JOptionPane.showMessageDialog(null, "Tipo Hora: " + tipoHora);
            }
        }
    }

}
