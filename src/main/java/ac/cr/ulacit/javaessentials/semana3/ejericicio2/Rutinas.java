/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.cr.ulacit.javaessentials.semana3.ejericicio2;

/**
 *
 * @author Dev1_JD
 */
public class Rutinas {

    public void ejemplo2(int a, int b, int c) {
        if (a > b) {
            if (a > c) {
                System.out.println("El numero mayor es a y vale " + a);
            } else {
                System.out.println("El numero mayor es c y vale " + c);
            }
        } else if (b > c) {
            System.out.println("El numero mayor es b y vale " + b);
        } else {
            System.out.println("El numero mayor es c y vale " + c);
        }
    }
}
