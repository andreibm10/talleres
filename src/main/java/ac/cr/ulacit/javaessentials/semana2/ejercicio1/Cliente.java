/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.cr.ulacit.javaessentials.semana2.ejercicio1;

/**
 *
 * @author Dev1_JD
 */
public class Cliente {

    private int id;
    private char clave;
    private double totalCompras;

    public Cliente() {
    }

    public Cliente(int id, char clave, double totalCompras) {
        this.id = id;
        if (clave == 'N' || clave == 'V') {
            this.clave = clave;
        } else {
            this.clave = '-';
        }
        this.totalCompras = totalCompras;
    }
    
    public void despliegaInfo(){
        System.out.println("ID " + id);
        System.out.println("Clave " + clave);
        System.out.println("Total de compras " + totalCompras);    
    }
}
