/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.cr.ulacit.javaessentials.semana2.ejercicio1;

/**
 *
 * @author Dev1_JD
 */
public class Prueba {
    public static void main(String[] args){
        //Probando cliente Nuevo
        Cliente clienteN = new Cliente(1, 'N', 1250);
        clienteN.despliegaInfo();
        
        //Probando cliente Viejo
        Cliente clienteV = new Cliente(1, 'V', 1250);
        clienteV.despliegaInfo();
        
        //Probando cliente Invalid clave es un '-'
        Cliente clienteSinClave = new Cliente(1, 'Z', 1250);
        clienteSinClave.despliegaInfo();
    }
}
