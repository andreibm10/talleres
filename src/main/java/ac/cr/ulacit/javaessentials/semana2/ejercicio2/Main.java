/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.cr.ulacit.javaessentials.semana2.ejercicio2;

/**
 *
 * @author Dev1_JD
 */
public class Main {

    public static void main(String[] args) {
        int a = 10;
        System.out.println("El valor de a es: " + a);
        int b, c, d, e;

        b = a++;
        System.out.println("El valor de a es: " + a + " El valor de b es: " + b);
        c = ++a;
        System.out.println("El valor de a es: " + a + " El valor de c es: " + c);
        d = a--;
        System.out.println("El valor de a es: " + a + " El valor de d es: " + d);
        e = --a;
        System.out.println("El valor de a es: " + a + " El valor de e es: " + e);

        System.out.println("El valor de a es: " + a);
        System.out.println("El valor de b es: " + b);
        System.out.println("El valor de c es: " + c);
        System.out.println("El valor de d es: " + d);
        System.out.println("El valor de e es: " + e);
    }
}
