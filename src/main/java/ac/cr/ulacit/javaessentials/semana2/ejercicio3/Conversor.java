/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.cr.ulacit.javaessentials.semana2.ejercicio3;

import javax.swing.JOptionPane;

/**
 *
 * @author Dev1_JD
 */
public class Conversor {

    public static void main(String[] args) {
        int f = Integer.parseInt(JOptionPane.showInputDialog("Digite la temperatura en F: "));
        System.out.println("La tempratura en Celcius es  " + String.valueOf(calculaCelsius(f)));
    }

    private static int calculaCelsius(int fahrenheit) {
        return ((fahrenheit - 32) * 5) / 9;
    }

    private static int calculaFahrenheit(int celcius) {
        return (celcius * 9) / (5 + 32);
    }
}
