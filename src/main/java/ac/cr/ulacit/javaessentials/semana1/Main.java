package ac.cr.ulacit.javaessentials.semana1;

import javax.swing.JOptionPane;

public class Main {

	public static void main(String[] args) {
		
                
                //Crear variable tipo Camisa llamada miCAmisa
		Camisa miCamisa;
		
		//Asignar a la variable miCamisa una nueva instancia de la clase Camisa
		miCamisa = new Camisa();
		
		//Llamar el metodo despliegaInfo de la camisa
		miCamisa.despliegaInfo();
		
	}

}
