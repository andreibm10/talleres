package ac.cr.ulacit.javaessentials.semana1;

public class Camisa {

	// Atributos
	private byte codigoId = 1;
	private String description = "t-shirt";
	private char codigoColor = 'R';
	public float precio = 0.008F;
	public byte cantidadEnBodega;

	// Contructor
	public Camisa() {
	}

	// Metodos (Comentario de una linea)
	/*
	 * Metodo que se encarga de desplegar la informacion de la camisa al usuario
	 * en el sistema (Comentario multilenea)
	 */
	public void despliegaInfo() {
		System.out.println("Codigo de camisa: " + codigoId);
		System.out.println("Descripcion: " + description);
		System.out.println("Color: " + codigoColor);
		System.out.println("Precio: " + precio);
		System.out.println("Cantidad en Bodega: " + cantidadEnBodega);
	}
}
